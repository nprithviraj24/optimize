#%%
import requests
import time
import concurrent.futures

img_urls = [
    'https://images.unsplash.com/photo-1516117172878-fd2c41f4a759',
    'https://images.unsplash.com/photo-1532009324734-20a7a5813719',
    'https://images.unsplash.com/photo-1524429656589-6633a470097c',
    'https://images.unsplash.com/photo-1530224264768-7ff8c1789d79',
    'https://images.unsplash.com/photo-1564135624576-c5c88640f235',
    'https://images.unsplash.com/photo-1541698444083-023c97d3f4b6',
    'https://images.unsplash.com/photo-1522364723953-452d3431c267',
    'https://images.unsplash.com/photo-1513938709626-033611b8cc03',
    'https://images.unsplash.com/photo-1507143550189-fed454f93097',
    'https://images.unsplash.com/photo-1493976040374-85c8e12f0c0e',
    'https://images.unsplash.com/photo-1504198453319-5ce911bafcde',
    'https://images.unsplash.com/photo-1530122037265-a5f1f91d3b99',
    'https://images.unsplash.com/photo-1516972810927-80185027ca84',
    'https://images.unsplash.com/photo-1550439062-609e1531270e',
    'https://images.unsplash.com/photo-1549692520-acc6669e2f0c'
]

def download_image(img_url):
    img_bytes = requests.get(img_url).content
    img_name = img_url.split('/')[3]
    img_name = f'{img_name}.jpg'
    with open(img_name, 'wb') as img_file:
        img_file.write(img_bytes)
        print(f'{img_name} was downloaded...')
#%%
t1 = time.perf_counter()

for img in img_urls:
	download_image(img)

t2 = time.perf_counter()

print(f'Without threading: Finished in {t2-t1} seconds')
''' Without threading: Finished in 185.06338758100173 seconds '''

#%%

t1 = time.perf_counter()
with concurrent.futures.ThreadPoolExecutor() as executor:
    # the following code will make requests asynchronously as opposed to synchronously in previous case
    executor.map(download_image, img_urls)
    # map takes func object and iterator as arguments
    
t2 = time.perf_counter()

print(f'With Threading: Finished in {t2-t1} seconds')
''' With Threading: Finished in 35.140544091002084 seconds '''

# This is a IO bound operation because we are waiting for the image to be downloaded (response from the site) 
# and its not moving to next url until we get response from the first one

# Threading can be useful when we are downloading because we are waiting lot of time
# Using threads, we can make request via url, and while this thread is waiting for response from website
# another thread is already making another request without waiting for the first one to complete (to get response back from the website)
# %%
## Examples of not IO bound operations
# - if we are downloading and resizing, then threads wont be an ideal operation here.
# in fact they will slow the whole process (because overhead of creating new threads)
# All these thread still are running in one processor
# - it can slow down our script
# However, we can make  whole operation fast by using multiprocessing.