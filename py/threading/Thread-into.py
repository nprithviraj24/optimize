#%%%
import threading

start = time.perf_counter()

def do_something():
	print('Sleeping 1 second ... ')
	time.sleep(1)
	print("Done Sleeping")


do_something()
do_something()

finish = time.perf_counter()
print(f'Finished in {round(finish-start, 2)} seconds(s)')
	 
#%%
'''Threading Basics'''
start = time.perf_counter()


# these are just definitions
t1 = threading.Thread(target = do_something)
t2 = threading.Thread(target = do_something)

# start our thread
t1.start()
t2.start()

# let our thread finish before proceeding to the rest of the code
t1.join()
t2.join()
# these both thread will take exactly 1 second to finish

finish = time.perf_counter()
print(f'Finished in {round(finish-start, 2)} seconds(s)')

#%%
'''Threads older way: Manual way'''
start = time.perf_counter()
def do_something2(seconds):
	print(f'Sleeping {seconds} seconds.. ')
	time.sleep(seconds)
	print("Done sleeping.. ")

threads = []
for _ in range(10):

	# threads take target as function object, and args as a list of args to that function
	t = threading.Thread(target = do_something2, args=[1.5])
	t.start()
	threads.append(t)

# we have to wait for each of these threads to finish
for thread in threads:
	thread.join()
finish = time.perf_counter()
print(f'Finished in {round(finish-start, 2)} seconds(s)')
# %%
'''Thread pool executor''' # from py3.2 

import concurrent.futures
start = time.perf_counter()
def do_something3(seconds):
	print(f'Sleeping {seconds} seconds.. ')
	time.sleep(seconds)
	return "Done sleeping.. "

# Context managers allow you to allocate and release resources precisely when you want to.
# The most widely used example of context managers is the "with" statement. 
with concurrent.futures.ThreadPoolExecutor() as executor:

	f1 = executor.submit(do_something3, 2) # submit method expect a func object and argument
	f2 = executor.submit(do_something3, 2) # submit method expect a func object and argument
	# submit method schedules a function to be executed and return a future object
	print(f1.result()) # it will till function complete
	print(f2.result()) # it will till function complete

finish = time.perf_counter()
print(f'Finished in {round(finish-start, 2)} seconds(s)')

# %%
'''Thread pool executor with list comprehensions'''

import concurrent.futures, time
start = time.perf_counter()

def do_something4(seconds):
	print(f'Sleeping {seconds} seconds.. ')
	time.sleep(seconds)
	return f"Done sleeping.. {seconds}"

with concurrent.futures.ThreadPoolExecutor() as executor:
	secs = [ 10, 5, 3, 1, 6 ] 
	results = [executor.submit(do_something4, sec) for sec in secs] 
	# submit method expect a func object and argument and it schedules a function to be executed and return a future object

	# in order to get these results concurrent futures have as completed function  which is a iterator
	# that we can use to loop over that will yield the results of our threads as they are completed
	for f in concurrent.futures.as_completed(results):
		print(f.result())

	# Notice that we are sending sec starting with 10, 5, 3.. 
	# but the results will be printed in with 1, 3, 5, 6, 10
	# which means that all the threads started at once, but their order of execution
	# depends on the how long they were sleeping that means the order they finish depends 
	# on how much time it took for them to execute


# Submit method submit each method at one time, and 
# following statement print only when all threads in threadpool are complete
finish = time.perf_counter()
print(f'Finished in {round(finish-start, 2)} seconds(s)')
# %%

'''Thread pool executor ons'''

import concurrent.futures, time
start = time.perf_counter()

def do_something4(seconds):
	print(f'Sleeping {seconds} seconds.. ')
	time.sleep(seconds)
	return f"Done sleeping.. {seconds}"

with concurrent.futures.ThreadPoolExecutor() as executor:
	secs = [  5, 10, 3, 1, 6 ] 
	results = executor.map(do_something4, secs) # we want to pass in our iterator as 2nd arg
	# map will run do_something4 for every value in our iterator i.e. secs
	# these threads will still run concurrently
	
	# these results will be returned in the order they were initiated
	for result in results:
		print(result) # if we print, they will be completed at same time as well
		# they will start at same time, but while returning they will return as they were started
		# so if 5s is started first, along with 10secs,.. both will start at same time
		# but 5s will return first.. and then 10s. The order of results matters when using map

	# Briefly when are sending sec starting with 5, 10, 3.. 
	# The first result will be printed after 5 seconds, rest of them will be printed after 10 seconds
	# All the threads start at once, but their order of print statements (or return values)
	# DOEST NOT depend on the how long they were sleeping (in previous case), but the sequence of 
	# the iterator that was sent in the map argument.
	# However, they will execute concurrently and will run the exact number of seconds that was given in argument 

# This section will run regardless of iterating over results iterator. 
# But this will only be executing after all the threads are executed : This is the essence of context manager

finish = time.perf_counter()
print(f'Finished in {round(finish-start, 2)} seconds(s)')

## About raise and error handling
# f our function (do_something4) raises an exception, it wont raise the exception while running the thread,
# it will raise when its value is retrieved from its results iterator 
# We can handle error inside the results iterator
# %%
