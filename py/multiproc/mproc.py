#%%
import time
def do_something():
	print('Sleeping for 1 second..')
	time.sleep(1)
	print('Done sleeping.. ')
#%%
'''Run synchronously (w/o) multiprocessing'''

start = time.perf_counter()
do_something()
do_something()
finish = time.perf_counter()
print(f'Finished in {round(finish-start, 2)} seconds')

#%%
# Threading on cpu bound task is can bring extra overhead ( because of creating multiple threads)
# it is not advisable to use threading on single processor for cpu bound tasks.
# file system operation and network base operations are ideal io bound operations which are ideal for threading

# with multiprocessing, we run multiple tasks at different processor at same time.
# we can use in io bound tasks and cpu bound tasks.
# - in multiprocessing processes do run at same time.

#%%
'''Older way of doing multiprocessing'''
import multiprocessing

start = time.perf_counter()
# Process expects a target argument which must be a function object, not return value of function
# remember target= do_something, 
p1 = multiprocessing.Process(target=do_something)
# target IS NOT do_something()

p2 = multiprocessing.Process(target=do_something)

# In order for our process to start and execute
p1.start()
p2.start()

# The above lines will start executing the programs without caring about rest of the program.
# Python will start those two processes and continue executing rest of the code while those 
# processes were still sleeping

# But if we want out processes to finish before we calculate the total run time
p1.join()
p2.join()

finish = time.perf_counter()
print(f'Finished in {round(finish-start, 2)} seconds')

# %%

''' Older way of multiple processing (more than 2 )'''

def do_something2(seconds):
	print(f'Sleeping for {seconds} second..')
	time.sleep(seconds)
	print('Done sleeping.. ')

start = time.perf_counter()
jobs = []
for _ in range(10):
	# Unlike threads, multiprocessing.Process expects arguments
	# that can be serialized using pickle
	p = multiprocessing.Process(target=do_something2, args=[2.2]) # 2.2 seconds sleep
	p.start()
	jobs.append(p)

for p in jobs:
	p.join()

finish = time.perf_counter()
print(f'Finished in {round(finish-start, 2)} seconds')

# We might not have 10 cores in computer, but computer figures out a way to execute 
# different core when that core is not too busy
# %%

'''Multiprocessing using concurrent.futures (Basic)'''
# Using process pool executor, which is much easier and efficient way 
# provides flexibility as we can switch to threading too if required

import concurrent.futures 
start = time.perf_counter()
def do_something3(seconds):
	print(f'Sleeping for {seconds} second..')
	time.sleep(seconds)
	return 'Done sleeping.. '

with concurrent.futures.ProcessPoolExecutor() as executor:
	# submit method schedules a func to be executed and return a future obj
	# future obj basically encapsulates the execution of a function and 
	# allows to check on it after its been scheduled, so we can check if its running, or if its done
	# and also we can check result which is return value of the function
	f1 = executor.submit(do_something3, 1)
	f2 = executor.submit(do_something3, 2)
	print(f1.result )# result is the future object
	print(f1.result()) # result is the future object
	print(f2.result()) # result is the future object

finish = time.perf_counter()
print(f'Finished in {round(finish-start, 2)} seconds')
#%%

start = time.perf_counter()
with concurrent.futures.ProcessPoolExecutor() as executor:
	results = [executor.submit(do_something3, 1) for _ in range(10)]
	# In order to get these results, we can use a function called as_completed
	# as_completed takes an iterator and makes it possible for us to loop over completed results.
	# as_Completed gives us a iterator that we can loop over that will yield the results of processes as they are completed
	for f in concurrent.futures.as_completed(results):
		print(f.result())

finish = time.perf_counter()
print(f'Finished in {round(finish-start, 2)} seconds')
#%%

def do_something4(seconds):
	print(f'Sleeping for {seconds} second..')
	time.sleep(seconds)
	return f'Done sleeping.. {seconds} '

start = time.perf_counter()
with concurrent.futures.ProcessPoolExecutor() as executor:
	secs = [5, 4, 3, 2, 1]
	results = [executor.submit(do_something4, sec) for sec in secs]
	# In order to get these results, we can use a function called as_completed
	# as_completed takes an iterator and makes it possible for us to loop over completed results.
	# as_Completed gives us a iterator that we can loop over that will yield the results of processes as they are completed
	for f in concurrent.futures.as_completed(results):
	# this will print out results as they were completed
		print(f.result())

finish = time.perf_counter()
print(f'Finished in {round(finish-start, 2)} seconds')
# %%
'''Multiprocessing over a iteratable'''

def do_something5(seconds):
	print(f'Sleeping for {seconds} second..')
	time.sleep(seconds)
	if seconds == 3: 
		raise
	return f'Done sleeping.. {seconds} '

start = time.perf_counter()
# with provides a context manager, and process in this context are automatically going to join
# and they are going to complete before context manager exits.
with concurrent.futures.ProcessPoolExecutor() as executor:
	secs = [5, 4, 3, 2, 1]
	results = executor.map(do_something5, secs)
	# basically what map does is it runs do_something4 method with every values in our iteratable i.e secs

	# map returns results in the order that they were started
	for result in results:
		print(result)
		# each result are displayed according to the order they were started
		# it DOES NOT mean that each process completed after its previous one was done.

finish = time.perf_counter()
print(f'Finished in {round(finish-start, 2)} seconds')
# Finished in 5.09 seconds

# IF our function raises an exception for a particular value, it wont actually raise that exception while running the process
# the exception will be raised when its value is retrieved   from results iterator
# Handling these execeptions needs to be done in the iterator where we are iterating over the results.

# %%
