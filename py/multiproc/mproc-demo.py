#%%
import time
from PIL import Image, ImageFilter 

import concurrent.futures

img_names = [
    'photo-1516117172878-fd2c41f4a759.jpg',
    'photo-1532009324734-20a7a5813719.jpg',
    'photo-1524429656589-6633a470097c.jpg',
    'photo-1530224264768-7ff8c1789d79.jpg',
    'photo-1564135624576-c5c88640f235.jpg',
    'photo-1541698444083-023c97d3f4b6.jpg',
    'photo-1522364723953-452d3431c267.jpg',
    'photo-1513938709626-033611b8cc03.jpg',
    'photo-1507143550189-fed454f93097.jpg',
    'photo-1493976040374-85c8e12f0c0e.jpg',
    'photo-1504198453319-5ce911bafcde.jpg',
    'photo-1530122037265-a5f1f91d3b99.jpg',
    'photo-1516972810927-80185027ca84.jpg',
    'photo-1550439062-609e1531270e.jpg',
    'photo-1549692520-acc6669e2f0c.jpg'
]

t1 = time.perf_counter()
size = (1200, 1200)
# Even if its IO bound, multibpund works well with io and cpu bound operations as well.
def process_img(img_name):
		img = Image.open(img_name) # io bound
		img = img.filter(ImageFilter.GaussianBlur(15)) # cpu bound
		img.thumbnail(size)
		img.save(f'processed/{img_name}') # io bound
		print(f'{img_name} was processed... ')


for img_name in img_names:
	process_img(img_name)

t2 = time.perf_counter()
print(f'Without multiprocessing: {round(t2-t1, 2)} seconds')
# Without multiprocessing: 30.53 seconds
#%%
'''Using multiple processors '''
import concurrent.futures as CF
t1 = time.perf_counter()

# with CF.ProcessPoolExecutor() as executor:
with CF.ThreadPoolExecutor() as executor: # testing with thread pool executor
	executor.map(process_img, img_names )
	# we dont need to store results as we dont have return type



t2 = time.perf_counter()
print(f' Multiprocessing : {round(t2-t1, 2)} seconds')
# Multiprocessing : 21.23 seconds
				
# %%
